import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import { AntDesign } from "@expo/vector-icons";

const ReservasScreen = ({ navigation }) => {
  const [selectedCourt, setSelectedCourt] = useState("");
  const [courtData, setCourtData] = useState({
    Tênis: {
      tipo: "Quadra de Tênis",
      capacidade: 4,
      taxaHora: 50,
      disponibilidade: "0",
    },
    Futebol: {
      tipo: "Campo de Futebol",
      capacidade: 22,
      taxaHora: 100,
      disponibilidade: "1",
    },
    Basquete: {
      tipo: "Quadra de Basquete",
      capacidade: 10,
      taxaHora: 80,
      disponibilidade: "1",
    },
    Vôlei: {
      tipo: "Quadra de Vôlei",
      capacidade: 12,
      taxaHora: 80,
      disponibilidade: "1",
    },
  });

  const handleCourtSelection = (court) => {
    setSelectedCourt(court);
  };

  const handleReservation = () => {
    console.log("Quadra:", courtData[selectedCourt].tipo);
    console.log("Capacidade de pessoas:", courtData[selectedCourt].capacidade);
    console.log("Valor por periodo:", courtData[selectedCourt].taxaHora);
    console.log("Disponibilidade:", courtData[selectedCourt].disponibilidade);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.textInput}>INFORMAÇÕES SOBRE AS QUADRAS</Text>
      <View style={styles.courtContainer}>
        <Picker
          style={styles.picker}
          selectedValue={selectedCourt}
          onValueChange={(itemValue) => handleCourtSelection(itemValue)}
        >
          <Picker.Item label="Selecione a quadra" value="" />
          <Picker.Item label="Quadra de Basquete" value="Basquete" />
          <Picker.Item label="Quadra de Vôlei" value="Vôlei" />
          <Picker.Item label="Campo de Futebol" value="Futebol" />
          <Picker.Item label="Quadra de Tênis" value="Tênis" />
        </Picker>
      </View>

      {selectedCourt !== "" && (
        <>
          <Text style={styles.label}>Tipo de Quadra</Text>
          <TextInput
            style={styles.input}
            placeholder="Tipo de Quadra"
            value={courtData[selectedCourt].tipo}
            editable={false}
          />
          <Text style={styles.label}>Capacidade de pessoas</Text>
          <TextInput
            style={styles.input}
            placeholder="Capacidade de pessoas"
            value={courtData[selectedCourt].capacidade.toString()}
            editable={false}
          />
          <Text style={styles.label}>Valor por período</Text>
          <TextInput
            style={styles.input}
            placeholder="Valor por período"
            value={courtData[selectedCourt].taxaHora.toString()}
            editable={false}
          />
          <Text style={styles.label}>Disponibilidade</Text>
          <TextInput
            style={styles.input}
            placeholder="Disponibilidade"
            value={courtData[selectedCourt].disponibilidade}
            editable={false}
          />
        </>
      )}

      <View style={styles.buttonContainer}>
        <Button
          title="RESERVAR"
          onPress={handleReservation}
          color="#3e945a"
          disabled={selectedCourt === ""}

        />
      </View>

      <TouchableOpacity
        style={styles.backButton}
        onPress={() => navigation.navigate("Home")}
      >
        <AntDesign name="arrowleft" size={24} color="#3e945a" />
      </TouchableOpacity>

      <View style={styles.footer}>
        <Text style={styles.footerText}>
          © 2024 Reserva de Quadras Esportivas
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#faf2f2",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 16,
    marginLeft: 10,
    marginRight: 10,
  },
  textInput: {
    fontSize: 25,
    fontWeight: "bold",
    marginBottom: 500,
    textAlign: "center",
  },
  label: {
    fontSize: 12,
    color: "#666",
    marginBottom: 5,
  },
  input: {
    borderColor: "#4dbf72",
    borderWidth: 3,
    borderRadius: 30,
    fontSize: 12,
    width: "80%",
    padding: 10,
    marginVertical: 3,
    marginTop: 3,
    marginBottom: 5,
    paddingHorizontal: 10,
    height: 40,
  },
  courtContainer: {
    borderRadius: 30,
    width: "80%",
    padding: 3,
    marginVertical: 10,
  },
  picker: {
    width: "100%",
    height: 40,
    color: "#333",
    fontSize: 12,
    borderRadius: 30,
    paddingHorizontal: 10,
    borderColor: "#4dbf72",
    borderWidth: 3,
  },
  buttonContainer: {
    backgroundColor: "#e6e6e6",
    borderRadius: 20,
    overflow: "hidden",
    width: "80%",
    marginBottom: 3,
    marginVertical: 10,
  },
  backButton: {
    position: "absolute",
    top: 40,
    left: 10,
    zIndex: 999,
  },
  footer: {
    position: "absolute",
    bottom: 0,
  },
  footerText: {
    fontSize: 12,
    color: "#666",
  },
});

export default ReservasScreen;

import axios from "axios";

const api = axios.create({
    baseURL: "http://10.89.234.178:5000/quadras",
    headers: {
        'Accept': 'application/json',
    },
});

const users = {
    postUser: (user) => api.post("/usuario", user),
    putUser: (user) => api.put("/usuario", user),
    deleteUser: (_id) => api.delete(`/usuario/${_id}`),
    logUser: (user) => api.post("/login", user),
}

export default users;

import React from "react";
import { StyleSheet, Text, View, Button, } from "react-native";


function PaginaInicial({ navigation }) {
    return (
        
        <View style={styles.container}>
            <Text style={styles.text}>BEM VINDO  RESERVAS ESPORTIVAS</Text>
            <View style={styles.buttonContainer}>
                <Button title="Login" onPress={() => navigation.navigate('Login')} color='#3e945a' />
            </View>
            <View style={styles.buttonContainer}>
                <Button title="Cadastre-se" onPress={() => navigation.navigate('Register')} color='#3e945a' />
            </View>
            <View style={styles.footer}>
        <Text style={styles.footerText}>© 2024 Reserva de Quadras Esportivas</Text>
      </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center", // Alinhar itens
        justifyContent: "center",
        backgroundColor: '#faf2f2',
        paddingHorizontal: 80,
    },
    buttonContainer: {
        backgroundColor: '#e6e6e6',
        borderRadius: 20,
        overflow: 'hidden',
        width: '80%', //  para reduzir o tamanho
        marginBottom: 1,
        marginTop: 4,
    },
    text: {
        height: 500,
        fontWeight: 'bold', // Coloca o texto em negrito
        marginBottom: 20,
        fontSize: 40,
        textAlign: "center", // Alinhar o texto
        alignSelf: "center", // Alinhar o texto 
        fontFamily: "System" // fonte do texto
    },
    bold: {
        fontSize: 20, // Tamanho da fonte
        fontWeight: "bold", // Texto em negrito
        fontFamily: "System", // Alterar a fonte 
    },
    footer: {
        position: "absolute",
        bottom: 0,
    },
    footerText: {
        fontSize: 14,
        color: "#666",
      },
});

export default PaginaInicial;
